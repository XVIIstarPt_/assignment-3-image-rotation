//
// Created by vaporplatinate on 11/18/23.
//
#include <stdio.h>
#include <stdlib.h>

#include "ANSI_colors.h"
#include "bmp_proc.h"
#include "rotator.h"
#include "utils.h"

#define READ_RIGHTS "rb"
#define WRITE_RIGHTS "wb"

char* read_messages[] = {
        [READ_INVALID_SIGNATURE] = REDHB "READ ERROR: invalid BMP header signature." RESET "\n",
        [READ_INVALID_BITS] = REDHB "READ ERROR: invalid BMP bits." RESET "\n",
        [READ_INVALID_HEADER] = REDHB "READ ERROR: invalid BMP header." RESET "\n"
};

char* write_messages[] = {
        [WRITE_INVALID_HEADER] = MAGENTAHB "WRITE ERROR: invalid header." RESET "\n",
        [WRITE_INVALID_BITS] = MAGENTAHB "WRITE ERROR: invalid bits." RESET "\n"
};

int main(int argc, char* argv[]) {
    if(argc != ARG_NUMBER){
        fprintf(stderr, HCYAN "Invalid usage: the correct usage would be: <source-image> <rotated-image> <angle>\n" RESET);
        return WRONG_ARGUMENT_EXIT_CODE;
    }

    const char *angle = argv[ANGLE_ARG_NUMBER];

    if(angle_is_valid(angle) == 0){
        fprintf(stderr, HCYAN "Invalid angle: only angles divisible by 90 (and obviously less or equal than 270 by absolute value because 360 == 0) are allowed.\n" RESET);
        return WRONG_ARGUMENT_EXIT_CODE;
    }

    FILE* src_file = fopen(argv[SRC_IMG_ARG_NUMBER], READ_RIGHTS);
    if(src_file == NULL) {
        printf(REDHB "Invalid file: file not found." RESET "\n");
        return NON_EXISTENT_FILE_EXIT_CODE;
    }

    struct image source;
    enum read_status r_stat = from_bmp(src_file, &source);

    if (r_stat != READ_OK) {
        printf("%s", read_messages[r_stat]);
        delete_image(&source);
        fclose(src_file);
        return READ_ERROR_EXIT_CODE;
    }

    int64_t angle_int = atoi(angle);

    FILE* dest_file = fopen(argv[DESTINATION_IMG_ARG_NUMBER], WRITE_RIGHTS);
    struct image destination = transform(source, angle_int);
    enum write_status w_stat = to_bmp(dest_file, &destination);
    delete_image(&destination);
    delete_image(&source);

    if(w_stat != WRITE_OK){
        printf("%s", write_messages[w_stat]);
        fclose(dest_file);
        fclose(src_file);
        return WRITE_ERROR_EXIT_CODE;
    }
    fclose(src_file);
    fclose(dest_file);
    printf(GREENHB "Transformation complete." RESET "\n");
    return 0;
}
