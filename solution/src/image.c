//
// Created by vaporplatinate on 11/18/23.
//
#include "ANSI_colors.h"
#include "image.h"
#include "utils.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool angle_is_valid(const char* angle){
    int angle_value = atoi(angle);
    int angles[] = {0, 90, 180, 270};
    if(angle_value < 0){
        angle_value += 360;
    }
    for(int i = 0; i < sizeof(angles); i++){
        if(angle_value == angles[i]){
            return true;
        }
    }
    return false;
}

struct image create_image(uint64_t width, uint64_t height){
    struct image img = {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    };

    if(img.data == NULL){
        printf(REDHB "ERROR: Out of memory" RESET);
        return (struct image){0};
    }
    return img;
}

void delete_image(struct image* img) {
    free(img->data);
}
