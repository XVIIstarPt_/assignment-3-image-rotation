//
// Created by vaporplatinate on 11/18/23.
//

#include "bmp_proc.h"

#define BMP_ALIGN 4
#define BMP_BF_RESERVED 0
#define BMP_BF_HEADER_SIZE 40
#define BMP_BIT_COUNT 24
#define BMP_COLORS_IMPORTANT 0
#define BMP_COLORS_USED 0
#define BMP_COMPRESSION 0
#define BMP_HEADER_PPM 2835
#define BMP_PLANES 1
#define BMP_TYPE 0x4D42

static uint8_t get_padding(uint8_t width){
    uint8_t row_size = width * sizeof(struct pixel);
    return (BMP_ALIGN - row_size % BMP_ALIGN) % BMP_ALIGN;
}
enum read_status from_bmp(FILE* IN, struct image* img){
    struct bmp_header header;
    if(fread(&header, sizeof(struct bmp_header), 1, IN) != 1){
        return READ_INVALID_HEADER;
    }
    if(header.bfType != BMP_TYPE){
        return READ_INVALID_SIGNATURE;
    }
    if(header.biBitCount != 24 || header.biCompression != 0) {
        return READ_INVALID_BITS;
    }
    *img = create_image(header.biWidth, header.biHeight);
    for(uint32_t ordinate = 0; ordinate < img -> height; ++ordinate){
        if(fread(&img->data[ordinate * header.biWidth], sizeof(struct pixel), header.biWidth, IN) != header.biWidth){
            return READ_INVALID_BITS;
        }
        fseek(IN, (long) get_padding(img -> width), SEEK_CUR);
    }
    return READ_OK;
}
enum write_status to_bmp(FILE* OUT, struct image const *img){
    uint32_t padding_size = get_padding(img -> width);
    uint32_t row_padded = img -> width * sizeof(struct pixel) + padding_size;
    struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfileSize = row_padded * img -> height + sizeof(struct bmp_header),
            .bfReserved = BMP_BF_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_BF_HEADER_SIZE,
            .biWidth = img -> width,
            .biHeight = img -> height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_BIT_COUNT,
            .biCompression = BMP_COMPRESSION,
            .biSizeImage = row_padded * img -> height,
            .biXPelsPerMeter = BMP_HEADER_PPM,
            .biYPelsPerMeter = BMP_HEADER_PPM,
            .biClrUsed = BMP_COLORS_USED,
            .biClrImportant = BMP_COLORS_IMPORTANT
    };
    if(fwrite(&header, sizeof(struct bmp_header), 1, OUT) != 1){
        return WRITE_INVALID_HEADER;
    }

    for(uint32_t ordinate = 0; ordinate < img -> height; ordinate++){
        if(fwrite(&img -> data[img -> width * ordinate], sizeof(struct pixel), img -> width, OUT) != img -> width){
            return WRITE_INVALID_BITS;
        }
        fseek(OUT, get_padding(img -> width), SEEK_CUR);
    }
    return WRITE_OK;
}
