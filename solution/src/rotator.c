//
// Created by vaporplatinate on 11/18/23.
//
#include "rotator.h"
#include "image.h"
#include <stdlib.h>
static struct image transform_copy(struct image const source){
    struct image transformed = create_image(source.width, source.height);
    if(transformed.data == NULL){
        return (struct image) {0};
    }
    for(uint64_t i = 0; i < source.width * source.height; i++){
        transformed.data[i] = source.data[i];
    }
    return transformed;
}

static struct image transform_right(struct image const source) {
    struct image transformed = create_image(source.height, source.width);
    if(transformed.data == NULL){
        return (struct image) {0};
    }
    for (uint64_t y = 0; y < source.height; ++y) {
        for (uint64_t x = 0; x < source.width; ++x) {
            uint64_t h = transformed.height - x - 1;
            transformed.data[h * transformed.width + y] = source.data[y * source.width + x];
        }
    }
    return transformed;
}

static struct image transform_australian(struct image const source) {
    struct image transformed = create_image(source.width, source.height);
    if(transformed.data == NULL){
        return (struct image) {0};
    }
    for (uint64_t y = 0; y < source.height; ++y) {
        for (uint64_t x = 0; x < source.width; ++x) {
            uint64_t h = transformed.width - x - 1;
            uint64_t w = transformed.height - y - 1;
            transformed.data[w * transformed.width + h] = source.data[y * source.width + x];
        }
    }
    return transformed;
}

static struct image transform_left(struct image const source) {
    struct image transformed = create_image(source.height, source.width);
    if(transformed.data == NULL){
        return (struct image) {0};
    }
    for (uint64_t y = 0; y < source.height; ++y) {
        for (uint64_t x = 0; x < source.width; ++x) {
            uint64_t w = transformed.width - y - 1;
            transformed.data[w + transformed.width * x] = source.data[y * source.width + x];
        }
    }
    return transformed;
}

typedef struct image (*f)(struct image const source);
f func[4] = {&transform_copy, &transform_right, &transform_australian, &transform_left};

struct image transform(struct image src, int64_t angle) {
    if(angle < 0) angle += 360;
    int64_t i = angle / 90;
    return func[i](src);
}
