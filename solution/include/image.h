//
// Created by vaporplatinate on 11/18/23.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct pixel{
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct image{
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct image create_image(uint64_t width, uint64_t height);

void delete_image(struct image* img);
#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
